﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using demo_linq_winform.classes;

namespace demo_linq_winform
{
    public partial class frm_member : Form
    {
        public frm_member()
        {
            InitializeComponent();
        }

        public group g = null;
        public member m = null;
        private void frm_member_Load(object sender, EventArgs e)
        {
            List<group> arr = GroupModels.getList();
            cboGroupId.ValueMember = "id";
            cboGroupId.DisplayMember = "name";
            cboGroupId.DataSource = arr;

            if (g != null)
            {
                cboGroupId.SelectedValue = g.id;
            }
            //Khi sua thong tin member
            if (m != null)
            {
                txtUsername.Text = m.username;
                txtPassword.Enabled = false;
                txtEmail.Text = m.email;
                txtFullname.Text = m.fullname;
                chkSex.Checked = m.sex == 1 ? true : false;
                cboGroupId.SelectedValue = m.group_id;
            }
        }
        //Kiểm tra rỗng
        string checkEmpty(TextBox txt, string name)
        {
            string error = "";
            if (txt.Text.Trim().Length == 0)
            {
                error += name + " không được rỗng\r\n";
            }
            return error;
        }
        //Hàm kiểm tra dữ liệu nhập
        string checkForm()
        {
            string error = "";
            //Tên đăng nhập không được rỗng
            error += checkEmpty(txtUsername, "Tên đăng nhập");
            if (m == null)
            {
                //Mật khẩu không được rỗng
                error += checkEmpty(txtPassword, "Mật khẩu");
            }
            //Email không được rỗng
            error += checkEmpty(txtEmail, "Email");
            return error;
        }
        string md5(string input)
        {
            System.Security.Cryptography.MD5CryptoServiceProvider x = new System.Security.Cryptography.MD5CryptoServiceProvider();
            byte[] bs = System.Text.Encoding.UTF8.GetBytes(input);
            bs = x.ComputeHash(bs);
            System.Text.StringBuilder s = new System.Text.StringBuilder();
            foreach (byte b in bs)
            {
                s.Append(b.ToString("x2").ToLower());
            }
            string password = s.ToString();
            return password;
        }
        private void btnSubmit_Click(object sender, EventArgs e)
        {
            //Kiểm tra dữ liệu nhập
            string error = checkForm();
            //Nếu hợp lệ
            if (error == "")
            {
                //---Thêm vào csdl
                my_databaseDataContext db = new my_databaseDataContext();

                member obj = new member();
                if (m != null)
                {
                    obj = (from r in db.members
                           where r.id == m.id
                           select r).FirstOrDefault();
                }
                obj.username = txtUsername.Text;
                if (m == null)//Thêm mới cho nhập password
                {
                    obj.password = md5(txtPassword.Text);
                }
                obj.email = txtEmail.Text;
                obj.fullname = txtFullname.Text;
                obj.sex = Convert.ToByte(chkSex.Checked ? 1 : 0);
                obj.group_id = Convert.ToInt32(cboGroupId.SelectedValue);
                if (m == null)
                {
                    db.members.InsertOnSubmit(obj);
                }
                string type = m == null ? "Thêm" : "Cập nhật";
                try
                {
                    db.SubmitChanges();
                    MessageBox.Show(type + " thành công");
                    if (m == null)
                    {
                        txtUsername.Clear();
                        txtPassword.Clear();
                        txtEmail.Clear();
                        txtFullname.Clear();
                    }
                    else
                    {
                        this.Close();
                    }
                }
                catch (Exception)
                {
                    MessageBox.Show(type + " thất bại");
                }
            }
            else
            {//Ngược lại
                //---Thông báo lỗi
                MessageBox.Show(error, "THONG BAO LOI", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
