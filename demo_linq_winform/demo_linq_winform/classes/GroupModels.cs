﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace demo_linq_winform.classes
{
    class GroupModels
    {
        public static List<group> getList()
        {
            my_databaseDataContext db = new my_databaseDataContext();
            var query = from r in db.groups
                        select r;
            return query.ToList<group>();
        }
    }
}
