﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace demo_linq_winform.classes
{
    class MemberModels
    {
        //Doc danh sach theo nhom
        public static List<member> getList(int group_id)
        {
            my_databaseDataContext db = new my_databaseDataContext();
            var query = from r in db.members
                        where r.group_id == group_id
                        orderby r.username
                        select r;
            return query.ToList<member>();
        }
        //Xoa
        public static void detele(int id)
        {
            my_databaseDataContext db = new my_databaseDataContext();
            var query = (from r in db.members
                        where r.id == id
                        select r).FirstOrDefault();
            db.members.DeleteOnSubmit(query);
            db.SubmitChanges();
        }
    }
}
