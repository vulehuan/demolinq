﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using demo_linq_winform.classes;

namespace demo_linq_winform
{
    public partial class frm_list_member : Form
    {
        public frm_list_member()
        {
            InitializeComponent();
        }

        private void frm_list_member_Load(object sender, EventArgs e)
        {
            List<group> arr = GroupModels.getList();
            lstGroup.DisplayMember = "name";
            lstGroup.DataSource = arr;
        }
        private void displayMemberByGroup(int index)
        {
            //group_id = id group dang duoc chon tren listbox
            group item = (group)lstGroup.SelectedItem;
            int group_id = item.id;
            //Loc member theo group_id
            List<member> arr = MemberModels.getList(group_id);
            dgvMember.DataSource = arr;
            if (dgvMember.Rows.Count > 0)
            {
                dgvMember.Columns["id"].Visible = false;
                dgvMember.Columns["group_id"].Visible = false;
                dgvMember.Columns["group"].Visible = false;

                dgvMember.Columns["username"].HeaderText = "Tên đăng nhập";
                dgvMember.Columns["password"].HeaderText = "Mật khẩu";
                dgvMember.Columns["fullname"].HeaderText = "Họ và tên";
                dgvMember.Columns["sex"].HeaderText = "Giới tính";

                if (dgvMember.Rows.Count > index)
                {
                    dgvMember.Rows[index].Selected = true;
                }
            }
        }
        private void displayMemberByGroup()
        {
            displayMemberByGroup(0);
        }
        private void lstGroup_SelectedIndexChanged(object sender, EventArgs e)
        {
            displayMemberByGroup();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            //Xac nhan xoa hay khong
            //Neu xoa
            if (MessageBox.Show("Bạn thật sự muốn xóa?", "XAC NHAN XOA", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
            {
                //---Xoa khoi csdl
                //------id = id cua member dang duoc chon
                member item = (member) dgvMember.SelectedRows[0].DataBoundItem;
                int id = item.id;
                //------Xoa dua vao id
                MemberModels.detele(id);
                //---Cap nhat lai du lieu tren form
                displayMemberByGroup();
            }
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            //show popup
            frm_member f = new frm_member();
            f.g = (group)lstGroup.SelectedItem;
            f.ShowDialog();
            //
            displayMemberByGroup();
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            //show popup
            frm_member f = new frm_member();
            int index = dgvMember.SelectedRows[0].Index;
            f.m = (member)dgvMember.SelectedRows[0].DataBoundItem;
            f.ShowDialog();
            //
            displayMemberByGroup(index);
        }
    }
}
