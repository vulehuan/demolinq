﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="add_member.aspx.cs" Inherits="add_member" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title></title>
    <style type="text/css">
        .addEditTable
        {
        }
        .addEditTable input
        {
            border: solid 1px #ccc;
        }
    </style>
</head>
<body>
    <form action="" method="post">
    <a href="list_member.aspx">Quay về</a>
    <%
        int id = Convert.ToInt32(Request.QueryString["id"]);
        member detail = MemberModels.getItem(id);
        if (detail == null)
        {
            detail = new member();
        }
     %>
    <table class="addEditTable">
        <tr>
            <td>
                Tên đăng nhập
            </td>
            <td>
                <input type="text" name="username" value="<%= detail.username %>" />
            </td>
        </tr>
        <%
            string action = Request.QueryString["action"];
            if (action != "edit")
            {
             %>
        <tr>
            <td>
                Mật khẩu
            </td>
            <td>
                <input type="password" name="password" />
            </td>
        </tr>
        <%
            }
             %>
        <tr>
            <td>
                Email
            </td>
            <td>
                <input type="text" name="email" value="<%= detail.email %>" />
            </td>
        </tr>
        <tr>
            <td>
                Họ và tên
            </td>
            <td>
                <input type="text" name="fullname" value="<%= detail.fullname %>" />
            </td>
        </tr>
        <tr>
            <td>
                Là nữ?
            </td>
            <td>
                <input type="checkbox" name="sex" value="1" <%= detail.sex == 1 ? " checked=\"checked\"" : "" %> />
            </td>
        </tr>
        <tr>
            <td>
                Nhóm
            </td>
            <td>
                <select name="group_id">
                    <%
                        System.Collections.Generic.List<group> arr = GroupModels.getList();
                        foreach (group obj in arr)
                        {
                    %>
                    <option value="<%= obj.id %>"<%= obj.id == detail.group_id ? " selected=\"selected\"" : "" %>>
                        <%= obj.name %></option>
                    <%
                    }
                    %>
                </select>
            </td>
        </tr>
        <tr>
            <td>
            </td>
            <td>
                <%
                    
                    string btnText = "Thêm";
                    if (action == "edit")
                    {
                        btnText = "Sửa";
                    }
                %>
                <input type="submit" name="submit" value=" <%= btnText %>" />
            </td>
        </tr>
    </table>
    </form>
</body>
</html>
