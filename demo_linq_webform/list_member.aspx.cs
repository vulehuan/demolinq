﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class list_member : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string action = Request.QueryString["action"];
        //Neu action la delete
        if (action == "delete")
        {
            //---Xoa
            int id = Convert.ToInt32(Request.QueryString["id"]);
            MemberModels.delete(id);
            Response.Redirect("list_member.aspx");
        }
    }
}