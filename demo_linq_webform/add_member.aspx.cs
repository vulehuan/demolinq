﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class add_member : System.Web.UI.Page
{
    //Ham kiem tra du lieu nhap
    //Neu gia tri tra ve khac rong ==> co loi xay ra
    string checkForm()
    {
        string error = "";
        //Ten dang nhap khong rong
        if (Request.Form["username"].Trim().Length == 0)
        {
            error += "Tên đăng nhập rỗng<br />";
        }
        string action = Request.QueryString["action"];
        if (action != "edit")
        {
            //Mat khau khong rong
            if (Request.Form["password"].Trim().Length == 0)
            {
                error += "Mật khẩu rỗng<br />";
            }
        }
        //Ho ten khong rong
        if (Request.Form["fullname"].Trim().Length == 0)
        {
            error += "Họ và tên rỗng<br />";
        }
        //Email khong rong
        if (Request.Form["email"].Trim().Length == 0)
        {
            error += "Email rỗng<br />";
        }
        return error;
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        //Neu submit duoc nhan
        if (Request.Form["submit"] != null)
        {
            //---Kiem tra du lieu nhap
            string error = checkForm();
            //---Neu hop le
            if (error == "")
            {
                string action = Request.QueryString["action"];
                if (action == "edit")
                {
                    //Sua
                    int id = Convert.ToInt32(Request.QueryString["id"]);
                    MemberModels.update(id);
                    Response.Redirect("list_member.aspx");
                }
                else
                {
                    //-----Them vao csdl
                    MemberModels.insert();
                    Response.Redirect("list_member.aspx");
                }
            }
            else
            {
                //Tu xu ly
            }
        }
    }
}