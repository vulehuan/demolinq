﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="list_member.aspx.cs" Inherits="list_member" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title></title>
    <style type="text/css">
        body
        {
            margin: 0;
            padding: 2px;
        }
        table.listTable
        {
            background: #000;
            border: 0;
        }
        table.listTable td, table.listTable th
        {
            background: #fff;
        }
        a
        {
            text-decoration: none;
            color: Maroon;
        }
        a:hover
        {
            color: #ff0000;
        }
    </style>
</head>
<body>
    <a href="add_member.aspx?action=add">Thêm</a>
    <table class="listTable" cellpadding="2" cellspacing="1">
        <tr>
            <th>
                STT
            </th>
            <th>
                Tên đăng nhập
            </th>
            <th>
                Email
            </th>
            <th>
                Họ và tên
            </th>
            <th>
                Thao tác
            </th>
        </tr>
        <%
            System.Collections.Generic.List<member> arr = MemberModels.getList();
            int i = 1;
            foreach (member obj in arr)
            {
        %>
        <tr>
            <td>
                <%= i %>
            </td>
            <td>
                <%= obj.username %>
            </td>
            <td>
                <%= obj.email %>
            </td>
            <td>
                <%= obj.fullname %>
            </td>
            <td>
                <a href="add_member.aspx?action=edit&id=<%= obj.id %>">Sửa</a> <a href="list_member.aspx?action=delete&id=<%= obj.id %>" onclick="return confirm('Bạn thật sự muốn xóa?')">Xóa</a>
            </td>
        </tr>
        <%
i++;
            }
        %>
    </table>
</body>
</html>
