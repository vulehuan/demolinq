﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for GroupModels
/// </summary>
public class GroupModels
{
	public GroupModels()
	{
	}
    //Lay danh sach group trong csdl
    public static List<group> getList()
    {
        my_databaseDataContext db = new my_databaseDataContext();
        var query = from r in db.groups
                    select r;
        return query.ToList<group>();
    }
}