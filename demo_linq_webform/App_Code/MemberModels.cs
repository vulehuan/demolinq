﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for MemberModels
/// </summary>
public class MemberModels
{
	public MemberModels()
	{
		
	}
    //Lay danh sach member trong csdl
    public static List<member>getList()
    {
        my_databaseDataContext db = new my_databaseDataContext();
        var query = from r in db.members
                    orderby r.username
                     select r;
        return query.ToList<member>();
    }
    //Xoa member
    public static void delete(int id)
    {
        my_databaseDataContext db = new my_databaseDataContext();
        var query = (from r in db.members
                    where r.id == id
                    select r).FirstOrDefault();
        db.members.DeleteOnSubmit(query);
        db.SubmitChanges();
    }
    public static string md5(string input)
    {
        System.Security.Cryptography.MD5CryptoServiceProvider x = new

System.Security.Cryptography.MD5CryptoServiceProvider();
        byte[] bs = System.Text.Encoding.UTF8.GetBytes(input);
        bs = x.ComputeHash(bs);
        System.Text.StringBuilder s = new System.Text.StringBuilder();
        foreach (byte b in bs)
        {
            s.Append(b.ToString("x2").ToLower());
        }
        string password = s.ToString();
        return password;
    }
    //Them member
    public static void insert()
    {
        my_databaseDataContext db = new my_databaseDataContext();
        member obj = new member();
        HttpRequest request = HttpContext.Current.Request;
        obj.username = request.Form["username"];
        obj.password = md5(request.Form["password"]);
        obj.email = request.Form["email"];
        obj.fullname = request.Form["fullname"];
        obj.group_id = Convert.ToInt32(request.Form["group_id"]);
        obj.sex = Convert.ToByte(request.Form["sex"] == null ? 0 : 1);
        db.members.InsertOnSubmit(obj);
        db.SubmitChanges();
    }
    //doc 1 member
    public static member getItem(int id)
    {
        my_databaseDataContext db = new my_databaseDataContext();
        var query = (from r in db.members
                     where r.id == id
                     select r).FirstOrDefault();
        return query;
    }
    //Sua thong tin
    public static void update(int id)
    {
        my_databaseDataContext db = new my_databaseDataContext();
        var obj = (from r in db.members
                     where r.id == id
                     select r).FirstOrDefault();
        HttpRequest request = HttpContext.Current.Request;
        obj.username = request.Form["username"];
        obj.email = request.Form["email"];
        obj.fullname = request.Form["fullname"];
        obj.group_id = Convert.ToInt32(request.Form["group_id"]);
        obj.sex = Convert.ToByte(request.Form["sex"] == null ? 0 : 1);
        
        db.SubmitChanges();
    }
}